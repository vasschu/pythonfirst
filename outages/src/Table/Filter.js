import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles((theme) => ({
	root: {
		'& > *': {
			margin: theme.spacing(1),
			width: '25ch',
		},
	},
}));

export default function Filter(props) {
	const classes = useStyles();
	const onChange = props.onChange;

	const handleChange = (ev) => {
		ev.preventDefault();

		const value = ev.target.value;
		onChange(value);
	};

	return (
		<form className={classes.root} noValidate autoComplete='off'>
			<TextField
				id='standard-basic'
				label={props.label}
				onChange={handleChange}
			/>
		</form>
	);
}
