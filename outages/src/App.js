import { useState, useEffect } from 'react';
import Table from './Table/Table';
import Filter from './Table/Filter';
import './App.css';

function App() {
	const [recentOutages, setRecentOutages] = useState('');
	const [isLoading, setIsLoading] = useState(true);
	const [filterService, setFilterService] = useState('');
	const [filterStartDate, setFilterStartDate] = useState('');
	const [displayData, setDisplayData] = useState('');

	async function fetchRecent() {
		const res = await fetch('http://localhost:8000/outages/recent/100');
		res
			.json()
			.then((res) => {
				if (!res.error) {
					setRecentOutages(res);
					setDisplayData(res);
					setIsLoading(false);
				} else {
					throw Error(`${res.error}`);
				}
			})
			.catch((err) => alert(err));
	}

	useEffect(() => {
		fetchRecent();
	}, []);

	useEffect(() => {
		let filteredData = recentOutages;

		if (filterService) {
			filteredData = recentOutages.filter((el) => {
				const checkValue = el.service_id.toString();
				if (checkValue.includes(filterService)) {
					return true;
				}
				return false;
			});
			setDisplayData(filteredData);
		}

		if (filterStartDate) {
			filteredData = recentOutages.filter((el) => {
				const checkValue = el.startTime.toString();
				if (checkValue.includes(filterStartDate)) {
					return true;
				}
				return false;
			});

			setDisplayData(filteredData);
		}
	}, [filterService, filterStartDate]);

	return (
		<div className='App'>
			<h1>Outages</h1>
			{isLoading ? (
				<h1>fetch data...</h1>
			) : (
				<>
					<Filter
						input={''}
						label={'filter service ID'}
						onChange={setFilterService}
					></Filter>
					<Filter
						input={''}
						label={'filter start date'}
						onChange={setFilterStartDate}
					></Filter>
					<Table rows={displayData}></Table>
				</>
			)}
		</div>
	);
}

export default App;
