from dateutil.parser import parse
from dateutil.relativedelta import relativedelta


def set_end_time(record):
    '''
    format incoming data to include end date
    param: outage record in dict format
    return: dict with endTime key and value
    '''
    time_parser = parse(record['startTime'])
    delta = relativedelta(minutes=record['duration'])
    end_time = time_parser + delta
    record['endTime'] = str(end_time)
    return record


def check_if_flapping(data):
    '''
    check if service is flapping
    param: list of records with outages for one service
    return: list of records satisfying the flapping criteria or false if no such records
    '''

    for record in data:
        start_time = parse(record['startTime'])
        time_range = start_time - relativedelta(minutes=120)

        records_within_range = filter(
            lambda el: parse(el['startTime']) >= time_range and parse(el['startTime']) <= start_time, data)

        records_within_range = list(
            map(lambda el: dict(el), records_within_range))

        if len(records_within_range) > 1 and sum(item['duration'] for item in records_within_range) >= 15:
            return records_within_range
        return False
