import numbers
from dateutil.parser import parse
from datetime import datetime


def validate_body(body):
    if not 'duration' in body or not isinstance(body['duration'], numbers.Number):
        return False
    if not 'service_id' in body or not isinstance(body['service_id'], numbers.Number):
        return False
    if not 'startTime' in body or not type(body['startTime']) == str:
        return False

    # validate date stirng format

    # parse_start_time = parse(body['startTime'])
    # if not isinstance(parse_start_time, datetime.date):
    #     return False

    return True
