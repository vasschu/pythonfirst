import requests
import datetime
from dateutil.parser import parse
import json
import random


# run python -m pytest tests\test.py


def test_flapping_scenarios_get_status_code_equals_200():
    response = requests.get("http://localhost:8000/outages/flapping/")
    assert response.status_code == 200


def test_response_contains_start_time_in_date_format():
    response = requests.get("http://localhost:8000/outages/flapping/").json()
    # response_body = list(response['result'])
    random_element = random.choice(response)
    start_time = parse(random_element['startTime'])

    assert isinstance(start_time, datetime.date)


def test_response_contains_sum_of_outages():
    response = requests.get("http://localhost:8000/outages/flapping/").json()
    random_element = random.choice(response)
    sum_of_duration = random_element['sumOfOutages']

    assert sum_of_duration >= 15


def test_response_contains_ammount_of_outages():
    response = requests.get("http://localhost:8000/outages/flapping/").json()
    random_element = random.choice(response)
    ammount_of_duration = random_element['ammountOfOutages']

    assert ammount_of_duration > 1
