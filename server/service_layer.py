import data_layer
from datetime import datetime
from dateutil.parser import parse
from dateutil.relativedelta import relativedelta
from common import set_end_time, check_if_flapping


def current_outages():
    '''
    return result / error object
    Logic what is defined for current outage is not clear to me, may refactor after clarification.
    Date is currently hardcoded for test purposes
    '''
    # now = datetime.now()
    now = parse('2020-08-20 00:00:00')
    five_minutes_back = now - relativedelta(minutes=5)

    current_time = now.strftime("%Y-%m-%d %H:%M:%S")
    end_time = five_minutes_back.strftime("%Y-%m-%d %H:%M:%S")

    current_outages = data_layer.current_outages(current_time, end_time)
    if current_outages:
        return {'error': 'null', 'result': current_outages}
    else:
        return {'error': 'No outages data found', 'result': 'null'}


def recent_outages(number_of_rows):
    '''
    param number_of_rows: specify the ammoun of recent records to return
    return result / error object
    '''
    outages = data_layer.recent_outages(number_of_rows)

    if outages:
        return {'error': 'null', 'result': outages}
    else:
        return {'error': 'No outages data found', 'result': 'null'}


def record_outage(data):
    '''
    param data: should be JSON in format {service_id: , duration: ,  startTime: <%Y-%M-%d %H:%M:%S>}
    return result / error object
    '''

    # add end date
    data_with_end_time = set_end_time(data)

    new_record = data_layer.record_outage(data_with_end_time)
    if new_record:
        return {'error': 'null', 'result': new_record}
    else:
        return {'error': 'Nothing was inserted', 'result': 'null'}


def ongoing_flapping_scenarios():
    '''
    return result / error object 
    in the code bellow the date is hardcoded to test against the available data.
    This must be changed with actual timestamp.
    '''
    # now = datetime.now()
    now = parse('2020-08-19 23:00:00')

    # checking only the last 5 hours
    five_hours_back = now - relativedelta(hours=5)

    timestamp = now.strftime("%Y-%m-%d %H:%M:%S")
    timestamp_five_hours_back = five_hours_back.strftime("%Y-%m-%d %H:%M:%S")

    recent_flapping_scenarios = data_layer.ongoing_flapping_scenarios(
        timestamp, timestamp_five_hours_back)

    if recent_flapping_scenarios:
        return {'error': 'null', 'result': recent_flapping_scenarios}
    else:
        return {'error': 'Ongoing flapping scenarios not found', 'result': 'null'}

    return recent_flapping_scenarios


def flapping_scenarios():
    '''
    return result / error object 
    '''

    # get ids of servecies that accumulated more than 15 min outages
    ids_repeated_outages = data_layer.ids_repeated_outages()

    # check if service accumulated 15 outages in two hour time frame
    flapping_list = []
    for id in ids_repeated_outages:
        outages_per_service = data_layer.outages_per_service(id['service_id'])
        flapping = check_if_flapping(outages_per_service)
        if flapping:
            flapping_list.append(flapping)

    # format response
    if flapping_list:
        response = []
        for service in flapping_list:
            record = {
                'service_id': service[0]['service_id'],
                'startTime': service[0]['startTime'],
                'duration': service[0]['duration'],
                'endTime': service[0]['endTime'],
                'ammountOfOutages': len(service),
                'sumOfOutages': sum(item['duration'] for item in service),
            }
            response.append(record)
        return {'error': 'null', 'result': response}
    else:
        return {'error': 'No flapping scenarios found', 'result': 'null'}
