from flask import Flask
import markdown
import os

app = Flask(__name__)


@app.route('/')
def index():
    """API documentation"""

    # Open the readme file
    with open(os.path.dirname(app.root_path) + '/README.md') as markdown_file:

        content = markdown_file.read()

        return markdown.markdown(content)
