import json
import sqlite3
from datetime import datetime
from dateutil.parser import parse
from common import set_end_time


conn = sqlite3.connect('outages.db')
c = conn.cursor()


def collect_data():
    '''
    import data from source file
    return imported JSON
    '''
    with open('outages.txt', 'r') as data_file:
        json_data = data_file.read()
    return json.loads(json_data)


# Create table
c.execute('''CREATE TABLE IF NOT EXISTS outages
             (id integer primary key autoincrement, service_id integer, duration integer, startTime text, endTime text)''')


def populate_db(data):
    '''
    Populate the database
    param: list of JSONS
    '''

    # set end time do the incoming data based on start time and duration
    data_with_end_time = list(map(set_end_time, data))

    c.executemany(
        'INSERT INTO outages (service_id, duration, startTime, endTime) VALUES (:service_id, :duration, :startTime, :endTime)', data_with_end_time)
    conn.commit()
    conn.close()


# run the code bellow to populate the database

# populate_db(collect_data())
