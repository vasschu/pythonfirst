import sqlite3
from datetime import datetime
from common import set_end_time


conn = sqlite3.connect('outages.db')
conn.row_factory = sqlite3.Row
c = conn.cursor()


def outages_per_service(id):
    c.execute(
        f"select * from outages where service_id = {id} order by startTime desc")
    return [dict(row) for row in c.fetchall()]


def current_outages(current_time, end_time):
    c.execute(
        f"select * from outages where (DATETIME(starttime) <= DATETIME('{current_time}') AND DATETIME(endtime) >= DATETIME('{end_time}'))")
    return [dict(row) for row in c.fetchall()]


def recent_outages(number_of_rows):
    c.execute(
        f'SELECT * FROM outages ORDER BY date(startTime) DESC Limit {number_of_rows}')
    return [dict(row) for row in c.fetchall()]


def record_outage(data):
    c.execute(
        'INSERT INTO outages (service_id, duration, startTime, endTime) VALUES (:service_id, :duration, :startTime, :endTime)', data)
    conn.commit()
    new_record_id = c.lastrowid
    c.execute(f'SELECT * FROM outages where id is {new_record_id}')
    return [dict(row) for row in c.fetchall()]


def identify_repeated_outages():
    c.execute('''select * from outages where service_id
    in ( SELECT service_id FROM outages group BY service_id having sum(duration) >= 15 and count(duration) > 1)
    order by starttime DESC''')
    return [dict(row) for row in c.fetchall()]


def ongoing_flapping_scenarios(timestamp, timestamp_five_hours_back):
    '''
    param timestamp: current date and hour
    param timestamp_five_hours_back: the timestamp with substracted 5 hours
    '''

    sql = f'''select service_id, duration, starttime, endtime, sum(duration) as sumOfOutages, count(duration) as amountOfOutages from outages
    where (DATETIME(starttime) <= DATETIME("{timestamp}") AND DATETIME(starttime) >= DATETIME("{timestamp_five_hours_back}")
    and service_id in ( SELECT service_id FROM outages group BY service_id having sum(duration) > 15 and count(duration) > 1))
    group by service_id having count(duration) > 1 and sum(duration)>=15'''
    c.execute(sql)
    return [dict(row) for row in c.fetchall()]


def ids_repeated_outages():
    c.execute('SELECT service_id from outages GROUP BY service_id HAVING sum(duration) >= 15 and count(duration) > 1;')
    return [dict(row) for row in c.fetchall()]
