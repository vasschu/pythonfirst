import hug
from hug.middleware import CORSMiddleware
from falcon import HTTP_400, HTTP_404


import service_layer
from validator import validate_body

api = hug.API(__name__)
api.http.add_middleware(CORSMiddleware(api))


@ hug.get('/outages/current')
def current_outages(response):
    content = service_layer.current_outages()
    if content['result']:
        return content['result']
    else:
        response.status = HTTP_404
        return {'error': content['error']}


@ hug.get('/outages/recent/{number_of_rows}')
def recent_outages(response, number_of_rows=10):
    '''
    param number_of_rows: specifing the number of recent records we would like to fetch
    '''
    content = service_layer.recent_outages(number_of_rows)
    if content['result']:
        return content['result']
    else:
        response.status = HTTP_404
        return {'error': content['error']}


@ hug.post('/outages')
def post_new_outage(body, response):
    '''
    request body: should be JSON in format {service_id: , duration: ,  startTime: <%Y-%M-%d %H:%M:%S>}
    '''

    if validate_body(body):
        content = service_layer.record_outage(body)
        if content['result']:
            return content['result']
        else:
            response.status = HTTP_404
            return {'error': content['error']}

    else:
        content = {
            'error': 'body should be JSON in format {service_id: , duration: ,  startTime: <%Y-%M-%d %H:%M:%S>'}
        response.status = HTTP_400
        return content


@ hug.get('/outages/flapping/')
def flapping_scenarios(response):
    content = service_layer.flapping_scenarios()
    if content['result']:
        return content['result']
    else:
        response.status = HTTP_404
        return {'error': content['error']}


@ hug.get('/outages/flapping/ongoing')
def ongoing_flapping_scenarios(response):
    content = service_layer.ongoing_flapping_scenarios()
    if content['result']:
        return content['result']
    else:
        response.status = HTTP_404
        return {'error': content['error']}


if __name__ == "__main__":
    api.http.serve()
